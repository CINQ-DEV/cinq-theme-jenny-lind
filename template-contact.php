<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<div id="primary" class="content-area container">
	<div class="row">
		<div class="col">

			<?php
			if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
			}
			?>
			<!-- end breadcrumbs -->
		</div>
	</div>
	<?php
	while (have_posts()) : the_post();
		?>
		<?php
		if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
			//the_post_thumbnail( 'full' );
		}
		?>
	<div class="row">
		<div class="col-12">
	<h1> <?php the_title(); ?></h1>
	<?php
	the_content();
	?>
		</div>
				<div class="col-12 col-lg-6 getting_here">
						<ul><h4>Our Details</h4>
							<li><i class="fas fa-map-marker-alt fa-lg"></i> <strong>Address</strong><br/><strong>The Jenny Lind Inn</strong>,<br> 69 High Street, Hastings, <br>East Sussex,<strong> TN34 3EW</strong></li>
							<li><i class="fas fa-phone-square fa-lg"></i> <strong>Telephone</strong><br> 01424 421392<br><br></li>
						</ul>
					<ul><h4>Getting Here</h4>
						<li><i class="fas fa-car fa-lg"></i> <strong>By Car</strong><br>
							<strong>London</strong> - 65 miles (approx 1hr 45min)<br>
							<strong>Brighton</strong> - 37 miles (approx 1hr 15min)<br>
							<strong>Ashford</strong> - 30 miles (approx 1hr)<br>
							Hastings Old town has limited on-street parking and car parks, unfortunetely we cannot reserve parking.<br><br>
						</li>
						<li><i class="fas fa-train fa-lg"></i> <strong> By Train</strong><br/>
							Hastings main line station is a mile away, with direct trains from London, Brighton & Ashford.
								<br><a href="https://www.thetrainline.com/" target="_blank">Check train times.</a><br><br>
							</li>
							<li>
					<i class="fas fa-taxi fa-lg"></i><strong> By Taxi</strong><br>
					Some local taxi company numbers, station is approx 1 mile away (cost approx £4)<br>
					24/7 Taxis - <strong>01424 247247</strong> <br>
					Phonix Taxis - <strong>01424 466466</strong></li>
				</ul>
				</div>
				<div class="col-12 col-lg-6 contact-form" >
		<?php $contactform = get_field("contact_form"); ?>
		<div id=”contact-form”>
				<h4>Contact Form </h4>
		<?php echo do_shortcode($contactform) ?>
		</div>
					</div>
		</div>
	</div>
<?php
// End of the loop.
endwhile;
?>
<div class="map-container">
	<?php require_once get_template_directory() . '/templates/functions_map.php'; ?>
</div>
<?php get_footer(); ?>
