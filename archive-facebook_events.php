<?php get_header(); ?>
<div id="primary" class="content-area container">
  <div class="row">
    <div class="col">
      <?php
      if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('<p id="breadcrumb">', '</p>');
      }
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col" style="border-top:1px solid black;padding-top:20px">
      <h1> <?php $post_type = get_post_type_object(get_post_type($post));
            //var_dump($post_type);

            ?>

        Events at the Jenny</h1>
    </div>
  </div>
  <div class="row">



    <?php
    // Start the loop.
    while (have_posts()) : the_post();
      $thumb_id = get_post_thumbnail_id();
      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
      $thumb_url = $thumb_url_array[0];

      //var_dump($post);
      ?><div class="col-6 grid">
        <div class="card">
          <img src="<?php echo $thumb_url ?>" class="card-img-top img-fluid" alt="<?php the_title() ?>">
          <div class="card-body">
            <h5 class="card-title"><?php the_title() ?></h5>
            <p class="card-text"><?php the_content() ?></p>
            <a href="<?php the_permalink() ?>" class="btn btn-primary">More Details</a>
          </div>
        </div>


      </div>
    <?php
  // End of the loop.
  endwhile;
  ?>

  </div>
</div>
<?php get_footer(); ?>