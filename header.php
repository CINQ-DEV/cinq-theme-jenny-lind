<!DOCTYPE html>
<html lang="en">
<?php $image = get_field('header_image'); ?>
<?php $size = get_field('header_size'); ?>
<html <?php language_attributes(); ?> class="no-js">
<head>
<?php wp_head(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-title" content="Jenny Lind">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/Icon_57.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/Icon_72.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/Icon_114.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/Icon_144.png">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
</head>
<body class="cinq">
  <header style="background-image: url('<?php echo $image['url']; ?>')" class="<?php echo $size ?>">
        <?php get_template_part('navigation'); ?>
        <?php require_once 'templates/functions_navigation.php';
        ?>
    </header>
    <a id="top"><i class="fas fa-chevron-up"></i></a>
    <div class="content">
