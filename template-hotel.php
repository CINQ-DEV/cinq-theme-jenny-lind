<?php /* Template Name: Hotel template */ ?>
<?php get_header(); ?>
<div id="primary" class="content-area container">
  <div class="row">
    <div class="col">

      <?php
      if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
      }
      ?>
      <!-- end breadcrumbs -->
    </div>
  </div>
    <div class="row">
      <div class="col-12 col-lg-12"><h1> <?php the_title(); ?></h1></div>
        <div class="col-12 col-lg-8">

            <?php the_field('content_left')?>
        </div>
          <div class="col-12 col-lg-4">  <?php the_field('content_right')?></div>

          <div class="col-12">
            <h2>Our Rooms </h2>

            <?php $shortcode = get_post_meta($post->ID,'room_list_shortcode',true);
            echo do_shortcode($shortcode);?>
          </div>
      </div>





    </div>
<?php get_footer(); ?>
