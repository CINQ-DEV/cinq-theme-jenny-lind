<?php get_header();
    /**
 * The template for displaying all single accomodation posts
 *

 */
 ?>
	<div id="primary" class="content-area">
<div class="container">
	<div class="row">
		<?php
        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<p id="breadcrumb">', '</p>');
        }
?>


	</div>


  <!-- Content here -->
	<?php
    // Start the loop.
    while (have_posts()) : the_post();
    ?>





  <div class="row">
		<div class="col-8">
			<h1>	<?php the_title(); ?></h1>
		<?php
        the_content();
        ?>
	</div>
	<div class="col-4"><h5>Features</h5>
  </div>
</div>
</div>
<?php
// End of the loop.
endwhile;
?>

<?php get_footer(); ?>
