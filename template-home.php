<?php /* Template Name: Homepage template*/ ?>
<?php get_header(); ?>
<div id="primary" class="content-area container">
<div class="row">

	<?php
	// Start the loop.
	  while (have_posts()) : the_post();
	?>

	<?php
	if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
	//the_post_thumbnail( 'full' );
	}
	?>
	  <?php
	      the_content();
	      ?>
	<?php
	// End of the loop.
	endwhile;
	?>



	</div>
<div class="row front_events">
		<div class="col-12">
			<h2 class="zebra">Whats On at the Jenny</h2></div>

		<?php echo do_shortcode('[add_eventon_list event_count="3" hide_past="yes" ux_val="3" hide_month_headers="yes" hide_mult_occur="yes" hide_empty_months="yes" show_year="no" tiles="yes" tile_count="3" tile_bg="1" tile_style="1" ]'); ?>

<a href="/whats-on" class="whats-on">View All events</a>
</div>

<!-- Bar Items -->
<div class="row front_bar">


	<div class="col-12">
		<h2 class="zebra">Glorious selection of Real Ales on tap.</h2>
	</div>
<div class="col-12 col-xl-4 beer_intro">
<p>We have no fewer than 10 hand-pumps at the bar, serving up classic cask ales plus short-term guest brews that just beg to be sampled.
	 We’re also well known for our traditional ciders and perries from all around the country. </p>

<p>It all adds up to make The Jenny Lind one of the <a href="/bars/">best pubs in Hastings for the beer lover</a>. If you want to try something new just ask our bar staff and they’ll be happy to recommend a drop.</p>

<?php //require_once 'templates/functions_beers_of_week.php';//beer of the week?>
</div>
<div class="col-12 col-xl-4">
<?php require_once 'templates/functions_beers_live_small.php';//beer of the week ?>
<?php// require_once 'templates/functions_beers_soon_small.php';//beer of the week ?>
</div>
<div class="col-12 col-xl-4">
<?php require_once 'templates/functions_beers_of_week.php';//beer of the week?>
<?php //require_once 'templates/functions_beers_coming_soon.php';//beer of the week?>
</div>
</div>
</div>
<?php get_footer(); ?>
