<div class="style_small_list">
	<?php $wcatTerms = get_terms('styles', array('hide_empty' =>  true, 'parent' =>0));
       foreach ($wcatTerms as $wcatTerm) :
       $term_id = $wcatTerm->term_id;?>
<?php
$ids = get_field('beers_on_sale', 'options');
$args = array(
    'post_type'          => 'ales',
    'post__in'            => $ids,
    'orderby'            => 'post__in',
        'tax_query' => array(
        array(
            'taxonomy' => 'styles',
            'field' => 'id',
            'terms' => $term_id
        )
            )
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post(); ?>
<div class="beer_item">
	<div class=" beer_text">
		<h4><?php the_title(); ?></h4><div class="pint"><span class="abv"><?php the_field('ale_abv') ?>%</span> £<?php the_field('ale_pint')?></div>
	</div>
</div>
	<?php
    }
} else {
    // no posts found
}
wp_reset_postdata();
?>
<?php endforeach; ?>
<a class="bar_more_info" href="/bars/">Find Out More</a>
</div>
