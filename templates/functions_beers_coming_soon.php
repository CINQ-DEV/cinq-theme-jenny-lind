<div class="featured-beer">
	<h3 class="zebra">Coming Soon</h3>
	<div class="row">
		<div class="col">

<?php
$ids = get_field('beers_coming_soon', 'options');
$args = array(
    'post_type'          => 'ales',
    'posts_per_page'    =>1,
    'post__in'            => $ids,
        'orderby'            => 'post__in',
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post(); ?>
			<?php the_post_thumbnail('logo'); ?>
	<h3 class="pisang"><?php the_title(); ?></h3>
	<?php the_field('ale_abv')?> % abv
<p>
<?php the_field('ale_tasting_notes')?>
</p>
				<div class="pint">£<?php the_field('ale_pint')?> |</div>
					<div class="half-pint">£<?php the_field('ale_half')?></div>
					<?php

$terms = get_the_terms($post->ID, 'features');
	if ( ! empty($terms)) {
        foreach ($terms as $term) {
          //  echo $term->name;
          //  echo $term->term_id;
            $term_id =  $term->term_id;
            $image = get_field('feature_icon', 'features' . '_' . $term_id); ?>
						<img class="icon_feature" src="<?php echo $image['url']; ?>" alt="<?php echo $term->name;?>" />


<?php
}  } ?>


	<?php
    }
} else {
    // no posts found
}
// Restore original Post Data
wp_reset_postdata();
?>
</div></div></div>
