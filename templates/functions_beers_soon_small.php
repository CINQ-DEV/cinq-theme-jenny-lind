<div class="style_small_list">
	  <h3 class="zebra" >Coming Soon</h3>
<!-- beers -->
<?php
$ids = get_field('beers_coming_soon', 'options');
$args = array(
    'post_type'          => 'ales',
    'post__in'            => $ids,
    'orderby'            => 'post__in',

);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post(); ?>
<div class="row beer_item">
	<div class="col-8 beer_text">
		<h3><?php the_title(); ?></h3>
	</div>
	<div class="col-4 ">
		<div class="pint">£<?php the_field('ale_pint')?></div>
	</div>
</div>
	<?php
    }
} else {
    // no posts found
}
wp_reset_postdata();
?>
<!-- beers -->

more info
</div>
