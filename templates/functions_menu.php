<?php
// Register Custom Navigation Walker
require_once get_template_directory() . '/templates/functions_bootstrap_navwalker.php';
//resolve wp to boostrap menu.
register_nav_menus(array(
    'menu_left' => __('Left Menu', 'THEMENAME'),
    'menu_right' => __('Right Menu', 'THEMENAME'),
    'legal' => __('Legal Menu', 'THEMENAME'),
    'mobile' => __('Mobile Menu', 'THEMENAME')
));

add_filter('wp_nav_menu_items', 'phone', 10, 2);
function phone($items, $args)
{
    if (is_page() && $args->theme_location == 'menu_') {
        $items .= '<i class="fas fa-phone-square fa-lg"></i>';
    }
    return $items;
}
add_filter('wp_nav_menu_items', 'accomodation', 10, 2);
function accomodation($items, $args)
{
    if (is_page() && $args->theme_location == 'menu_') {
        $items .= '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116 nav-item"><a href="#footer-anchor" class="book-now nav-link"> <i class="fas fa-bed fa-lg" id="book-now"></i> Book </a></li>';
    }
    return $items;
}
