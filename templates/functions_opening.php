<?php $date = date('d-m-Y ');//get current day
$nameOfDay = date('D', strtotime($date));
?>
<h2 class="widgettitle">Opening Hours</h2>
<?php
// check if the repeater field has rows of data
if (have_rows('opening_hours', 'option')):
    // loop through the rows of data
    while (have_rows('opening_hours', 'option')) : the_row();
        // display a sub field value
                ?>
                <?php $daycheck = get_sub_field('day_name', 'option');?>
				<div class="day check_<?php echo  strcmp($daycheck, $nameOfDay)?>">
			<div class="day_name " ><?php
        the_sub_field('day_name', 'option');?></div>
	<div class="time_open">
		<?php	the_sub_field('day_start', 'option');?> - <?php the_sub_field('day_end', 'option');?>
	</div>
</div>
<?php
    endwhile;
else :
    // no rows found
endif;
?>
