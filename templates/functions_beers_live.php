<?php $wcatTerms = get_terms('categories', array('hide_empty' =>  true, 'parent' => 0));
foreach ($wcatTerms as $wcatTerm) :
    ?>
	<?php
    ?>
	<div class="style <?php echo $wcatTerm->slug; ?>">
		<h2 class="zebra">
			<a href="<?php echo get_term_link($wcatTerm->slug, $wcatTerm->taxonomy); ?>">
				<?php echo $wcatTerm->name; ?>
				<?php
                ?>
			</a>
		</h2>
		<?php $term_id = $wcatTerm->term_id; ?>
		<!-- beers -->
		<?php
        $ids = get_field('beers_on_sale', 'options');
        $args = array(
            'post_type'          => 'ales',
            //'posts_per_page'    => 3,
            'post__in'            => $ids,
            //'orderby'            => 'post__in',
            'tax_query' => array(
                array(
                    'taxonomy' => 'categories',
                    'field' => 'id',
                    'terms' => $term_id
                )
            )
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post(); ?>
				<div class="row beer_item">
					<div class="col-12 col-lg-3 beer_thumb_col d-none d-xl-block">
						<?php the_post_thumbnail('logo', array('class' => 'beer_logo')); ?>
						<br/>
						<div class="abv"><?php the_field('ale_abv') ?>% abv </div>
					</div>
					<div class="col-12 col-lg-8 beer_text">
						<h3 class="pisang"><?php the_title(); ?></h3>
						<?php //Brewers
                        $terms = get_the_terms($post->ID, 'styles');
                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        ?>
								<span class="features">
									<?php echo $term->name; ?>
								</span>
							<?php
                    }
                } ?>
<span style="float:right">
						<div class="pint">£<?php the_field('ale_pint') ?> </div>
						</span>
						<p>
							<?php the_field('ale_tasting_notes') ?>
						</p>
						<div class="details">
							<?php
                            $terms = get_the_terms($post->ID, 'features');
                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        ?>
									<span class="features">
										<?php echo $term->name; ?>
									</span>
									<?php
                                    //  echo $term->term_id;
                                    $term_id =  $term->term_id;
                        $image = get_field('feature_icon', 'features' . '_' . $term_id); ?>
								<?php
                    }
                } ?>
							<span class="address">
								<?php //Brewers
                                $terms = get_the_terms($post->ID, 'brewers');
                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        ?>
										<span class="features">
											<?php echo $term->name; ?> </span>
										<?php $term_id =  $term->term_id; ?>
										<?php $location = get_field('brewers_location', 'brewers_' . $term_id);
                                        //echo $term_id;
                                        ?>
										<?php $town = get_field('brewers_town', 'brewers_' . $term_id);
                        echo $town ?>, <?php $county = get_field('brewers_county', 'brewers_' . $term_id);
                        echo $county ?>
										<?php if (!empty($location)) {
                            ?>
											<?php $address = explode(",", $location['address']);
                                            // var_dump($address);
                                            //echo $address[2];
                                            //echo $address[3];
                        }; ?>
									</span>
								<?php
                    }
                } ?>
						</div>
					</div>
				</div>
			<?php
            }
        } else {
            // no posts found
        }
    wp_reset_postdata();
    ?>
		<!-- beers -->
	</div>
<?php endforeach; ?>
