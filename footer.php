<a name="#footer-anchor"></a>
<footer class="footer" id="footer-anchor">
  <div class="container">
    <div class="row" id="contact" >
	<div class="col-md-4 col-12 contact" >
    <p>The Jenny Lind Inn<br>
      69 High St, Hastings, <br>
      East Sussex, tn34 3ew<br>
      <a class="call_me" href="tel:+44-1424-421392"><i class="fas fa-phone"></i> 01424 421392</a>
  </p>
	</div>
	<div id="booking-form" class="col-md-8 col-12 footer-right">
		<?php dynamic_sidebar('f2'); ?>
</div>
  </div>
  </div>
</footer>
<!-- Strap -->
<div id="bottom-strap" >
<div class="container">
<div class="row"  >
  <div class="col-12 col-md-4 center">
    <div class="opening">
    <?php  require_once get_template_directory() . '/templates/functions_opening.php';?>
    </div>
</div>
	<div class="col-12 col-md-4 center">
    <div class="legal">
    <?php dynamic_sidebar('f1'); ?>
  </div>
		</div>
	<div class="col-12 col-md-4 center">
    <div class="social-area">
		<?php  require_once get_template_directory() . '/templates/functions_social.php';?>
  </div>
  <div class="cards-area">
<i class="fab fa-cc-visa fa-2x "></i>
<i class="fab fa-cc-mastercard fa-2x "></i>
<i class="fab fa-cc-discover  fa-2x "></i>
</div>
</div>
  </div>
<div class="row">
  <div class="col-12 copyright-area">
    &copy; 2019 KC Pub Co Limited | Registered in England 07308089.
</div>
</div>
<!-- Strap -->
<?php wp_footer(); ?>
</body>
  </html>
