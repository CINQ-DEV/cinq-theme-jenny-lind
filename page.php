<!-- Default Single page template -->
<?php get_header(); ?>
<div id="primary" class="content-area container">

    <div class="row">
      <div class="col">

        <?php
        if (function_exists('yoast_breadcrumb')) {
          yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
        }
        ?>
        <!-- end breadcrumbs -->
      </div>
    </div>
    <div class="row">
    <div class="col-12">
  <h1> <?php the_title(); ?></h1>

    </div>
  </div>


  <div class="row">
  <div class="col-12">
            <?php
            // Start the loop.
            while (have_posts()) : the_post();
                ?>



                <?php
                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                    //the_post_thumbnail( 'full' );
                }
                ?>

                <?php
                the_content();
                ?>



            <?php
        // End of the loop.
        endwhile;
        ?>
      </div>
    </div>
  






</div>


<?php get_footer(); ?>
